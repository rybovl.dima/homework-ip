document.getElementById('find-ip-btn').addEventListener('click', async () => {
    const resultDiv = document.getElementById('result');
    resultDiv.textContent = 'Завантаження...';

    try {
        const ipResponse = await fetch('https://api.ipify.org/?format=json');
        const ipData = await ipResponse.json();
        const ipAddress = ipData.ip;

        const locationResponse = await fetch(`http://ip-api.com/json/${ipAddress}`);
        const locationData = await locationResponse.json();

        if (locationData.status === 'fail') {
            resultDiv.textContent = 'Не вдалося отримати інформацію про фізичну адресу';
        } else {
            resultDiv.innerHTML = `
                <p>Континент: ${locationData.continent}</p>
                <p>Країна: ${locationData.country}</p>
                <p>Регіон: ${locationData.regionName}</p>
                <p>Місто: ${locationData.city}</p>
                <p>Район: ${locationData.district || 'Невідомо'}</p>
            `;
        }
    } catch (error) {
        resultDiv.textContent = 'Сталася помилка при отриманні даних';
    }
});
